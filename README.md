# Desafio Técnico Front-End Pleno/Sênior

Olá! 😊
Seguem as instruções para a próxima fase do nosso processo seletivo da Datum TI em parceria com a Softplan!
Crie um projeto utilizando React e a API Graph Countries (https://github.com/lennertVanSever/graphcountries) seguindo as especificações abaixo.

## Funcionalidades esperadas:

- Crie uma lista de cards para exibir os países mostrando a bandeira, o nome e a capital dele;
- Possibilite o usuário buscar países;
- Na lista, o usuário pode ir para a página de detalhes do país e ver uma lista mais completa de informações (bandeira, nome, capital, área, população e top-level domain);
- Crie um formulário para editar os dados de um país (salvando apenas no client-side);

## Restrições técnicas:

- Utilize o create-react-app como base;
- Utilize redux para gerenciar o estado;
- Utilize react-router para trocar de página;
- Utilize @testing-library/react para testes;

## **Diferenciais**:

- Crie uma pipeline no GitLab; (Exemplo: build => test => deploy);
- Entregar o projeto publicado e funcionando em alguma URL;
- Garanta 100% de cobertura no projeto com testes unitários;
- Substituir o redux pelo Local state management do Apollo Client;

## **Desafio Super Front**:

- Na tela de detalhes do país, adicionar um mapa mostrando a distância entre o país e os 5 países mais próximos;

# Considerações do dev

### Hospedagem no Netlify

[![Netlify Status](https://api.netlify.com/api/v1/badges/20ff5e3e-6567-4c53-9589-c703e1e22d2f/deploy-status)](https://app.netlify.com/sites/countries-library-code-challenge/deploys)

## Pipeline de CI/CD no GitLab

https://gitlab.com/hladf/code-challenge-datum-countries-list/-/pipelines

## Rodar projeto

> OBS: Estou tendo problema de CORS ou algo do tipo pra chamar a api pelo site hospedado, só está funcionando localmente.  
> URL: https://countries-library-code-challenge.netlify.app/

Para rodar o projeto, basta rodar o seguinte comando na raíz do projeto:
`yarn && yarn start`

Para rodar testes:
`yarn test`

## Arquitetura

Pensei em aplicar um clean ou algo parecido, mas ficaria muito "Overengineering", então simplifiquei um pouco a arquitetura mantendo boa parte do código compartilhado na pasta `main`, como helpers, graphql/operations, router, config...  
Na pasta `presentation` deixei apenas o código mais relacionado á exibição dos dados e ao react e estilizações  
Na pasta `infra` ficou o acesso e configuração da lib do GraphQL  
Na pasta `models` deixei as definições de tipos importantes pro sistema  

## Código

Para organização e padronização do código apliquei eslint, prettier e order-imports. Também por isso que gosto e tento seguir ao máximo princípios como KISS, DRY, YAGNI (não da pra seguir tanto YAGNI num code-challenge 🤣), programação funcional, entre outros presentes no guia Object Calisthenics.

## Comentários

Tenho domínio e mais experiência com redux, porém por ser um ponto diferencial de avaliação, aceitei o desafio de deixar ele de lado e tentar aplicar 100% GQL.
Por não dominar tanto o GQL, acabei não testando tanto quanto gostaria, até pelo tempo de entrega podendo se extender também.
Adoro adicionar animações na aplicação, mas não pude dar prioridade pra isso dessa vez. 🙁
