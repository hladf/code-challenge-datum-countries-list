import { ApolloClient, InMemoryCache } from '@apollo/client';

import { GRAPHQL_URI } from 'main/config';
import { GET_LOCAL_COUNTRIES } from 'main/graphql/operations/queries';
import { mergeWithoutDuplicates } from 'main/helpers';
import { CountryDetailsModel, LocalCountriesListModel } from 'models';

const cache = new InMemoryCache();

export const GraphQlClient = new ApolloClient({
  uri: GRAPHQL_URI,
  cache,
  connectToDevTools: true,
  resolvers: {
    Mutation: {
      editCountry: (_, { country }: { country: CountryDetailsModel }, { cache }) => {
        const query = GET_LOCAL_COUNTRIES;

        const previous: LocalCountriesListModel = cache.readQuery({ query });

        const mergedValues = !previous?.localCountries.length
          ? [country]
          : mergeWithoutDuplicates(previous.localCountries, country, 'numericCode');

        const data = {
          localCountries: mergedValues,
        };

        cache.writeQuery({ query, data });
        return data;
      },
    },
  },
});
