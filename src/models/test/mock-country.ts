import { CountryModel } from 'models/country.model';

export const mockCountryList: CountryModel[] = [
  {
    name: 'Brazil',
    numericCode: '076',
    capital: 'Brasília',
    flag: {
      svgFile: 'https://restcountries.eu/data/bra.svg',
    },
  },
  {
    name: 'British Indian Ocean Territory',
    numericCode: '086',
    capital: 'Diego Garcia',
    flag: {
      svgFile: 'https://restcountries.eu/data/iot.svg',
    },
  },
  {
    name: 'Brunei Darussalam',
    numericCode: '096',
    capital: 'Bandar Seri Begawan',
    flag: {
      svgFile: 'https://restcountries.eu/data/brn.svg',
    },
  },
];
