export type CountryModel = {
  name: string;
  numericCode: string;
  capital: string;
  flag: Flag;
};

export type CountriesResultModel = {
  countries: CountryModel[];
};

type Flag = {
  svgFile: string;
};

export type QueryGetCountriesByNameVariables = {
  name?: string;
};

export type SelectedCountryModel = {
  numericCode: string;
};

export type CountryDetailsModel = {
  name: string;
  numericCode: string;
  capital: string;
  area: number;
  population: number;
  flag: Flag;
  topLevelDomains: TopLevelDomain[];
};

type TopLevelDomain = {
  name: string;
};

export type LocalCountriesListModel = { localCountries: CountryDetailsModel[] };
