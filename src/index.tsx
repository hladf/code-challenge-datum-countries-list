import { ApolloProvider } from '@apollo/client';
import React from 'react';
import ReactDOM from 'react-dom';

import { ThemeProvider } from 'styled-components';

import { GraphQlClient } from 'infra/graphql';
import { MainRouter } from 'main/router';
import GlobalStyle from 'presentation/styles/global';
import { theme } from 'presentation/styles/theme';

import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={GraphQlClient}>
      <ThemeProvider theme={theme}>
        <main>
          <MainRouter />
        </main>
        <GlobalStyle />
      </ThemeProvider>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
