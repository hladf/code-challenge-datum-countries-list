const colors = {
  primary: '#0976DB',
  secondary: '#0761B5',
  mediumBlue: '#053F75',
  lightBlue: '#0B83F5',
  darkGray: '#536162',
  black: '#000000',
  lightGray: '#eeeeee',
  white: '#ffffff',
};

const borderRadius = {
  primary: '8px',
  secondary: '4px',
};

const boxShadows = {
  primaryGray: `0px 3px 6px ${colors.darkGray}33`,
  secondaryStrong: `0px 3px 6px ${colors.black}`,
};

const fontSizes = {
  12: '0.75rem',
  14: '0.875rem',
  16: '1rem',
  20: '1.25rem',
};

export const theme = {
  breakpoints: {
    xs: 0,
    sm: 576,
    md: 768,
    lg: 992,
  },
  fontFamily: 'Inter, sans-serif',
  colors,
  boxShadows,
  borderRadius,
  fontSizes,
};

export type Theme = typeof theme;
