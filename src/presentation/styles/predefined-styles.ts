import { css } from 'styled-components';

export const borderAndShadowPrimary = css`
  border-radius: ${({ theme }) => theme.borderRadius.primary};
  box-shadow: ${({ theme }) => theme.boxShadows.primaryGray};
`;

export const borderAndShadowStrong = css`
  border-radius: ${({ theme }) => theme.borderRadius.primary};
  box-shadow: ${({ theme }) => theme.boxShadows.secondaryStrong};
`;

export const hoverEffectPrimary = css`
  &:hover {
    transition: transform 0.1s linear;
    transform: scale(1.01);
  }
`;
