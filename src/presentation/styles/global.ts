import { createGlobalStyle } from 'styled-components';

import { borderAndShadowPrimary } from './predefined-styles';

const GlobalStyle = createGlobalStyle`

*,
*::before,
*::after {
    font-family: ${({ theme }) => theme?.fontFamily || 'Inter, sans-serif'};
}

ul, li {
    list-style: none;
    padding: 0;
    margin: 0;
}

h1, h2, h3, h4, h5 {
  margin: 0;
}

a {
  text-decoration: none;
}

html {
  box-sizing: border-box;
}

body {
  background-color: ${({ theme }) => theme?.colors.darkGray || '#eee'}10;
  background-image: linear-gradient(to bottom left, #eee, ${({ theme }) =>
    theme?.colors.darkGray || '#eeeeee'}99);
  background-repeat: no-repeat;

  font-family: ${({ theme }) => theme?.fontFamily || 'Inter, sans-serif'};
  margin: 0;
  min-height: 95vh;
  height: 100%;
  padding-bottom: 20px;

  ::-webkit-scrollbar {
    width: 6px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.darkGray}50;
    border-radius: 3px;
    width: 6px;
  }

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.2);
  }
}

input {
  ${borderAndShadowPrimary}
    border-radius: ${({ theme }) => theme.borderRadius.secondary};
    border: 1px solid ${({ theme }) => theme.colors.lightGray};
    padding: 10px;
    outline: none;
}

button {
  ${borderAndShadowPrimary}
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  border-radius: ${({ theme }) => theme.borderRadius.secondary};
  border: none;
  box-sizing: border-box;
  padding: 10px;
  background-color: ${({ theme }) => theme.colors.lightBlue};
  color: ${({ theme }) => theme.colors.white};

  &:hover {
    font-weight: 500;
    background-color: ${({ theme }) => theme.colors.mediumBlue};
    color: ${({ theme }) => theme.colors.white};
  }
}

`;

export default GlobalStyle;
