export const Images = {
  MountainsLandscape: require('./images/rio_de_janeiro_brazil_mountains.jpg').default,
  GlobeIcon: require('./svg/globe-icon.svg').default,
  LupeIcon: require('./svg/lupe-icon.svg').default,
  EditIcon: require('./svg/edit-icon.svg').default,
  LoadingIcon: require('./images/loading_icon.png').default,
};
