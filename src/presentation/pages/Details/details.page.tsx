import { useQuery } from '@apollo/client';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { GET_COUNTRY_DETAILS } from 'main/graphql/operations/queries';
import { formatNumberToUS } from 'main/helpers';
import { mainRoutes } from 'main/router';
import { CountryDetailsModel } from 'models';
import { Images } from 'presentation/assets';
import { Header, Loading, RoundedImage } from 'presentation/components';
import { useSelectedCountry } from 'presentation/hooks';

import {
  DetailsContainer,
  EditButton,
  MainInfoContainer,
  SecondaryInfoContainer,
} from './details.styles';
import DetailsForm from './edit-details-form';

export const Details: React.FC = () => {
  const { push } = useHistory();
  const [isEditMode, setEditMode] = useState<boolean>(false);
  const {
    getSelectedCountryCode,
    setSelectedCountryLocalData,
    getSelectedCountryLocalData,
    refetchLocalCountries,
  } = useSelectedCountry();
  const { data: detailsData, loading } = useQuery<{
    countryDetails: CountryDetailsModel[];
  }>(GET_COUNTRY_DETAILS, {
    variables: {
      numericCode: getSelectedCountryCode?.numericCode || '',
    },
  });

  const selectedCountryLocalData = getSelectedCountryLocalData(
    getSelectedCountryCode?.numericCode || '',
  );

  const handleSubmitEdition = (values: CountryDetailsModel) => {
    setSelectedCountryLocalData(values);
    refetchLocalCountries();
    setEditMode(false);
  };

  const parsedDetailsData = selectedCountryLocalData || detailsData?.countryDetails[0];

  useEffect(() => {
    if (!getSelectedCountryCode?.numericCode) {
      push(mainRoutes.Main);
    }
  }, [getSelectedCountryCode]);

  if (loading || !parsedDetailsData) return <Loading />;

  return (
    <>
      <Header />
      <DetailsContainer>
        <EditButton
          src={Images.EditIcon}
          alt="Edit"
          width="1.5rem"
          onClick={() => setEditMode(!isEditMode)}
        />
        <MainInfoContainer>
          <RoundedImage
            src={parsedDetailsData.flag.svgFile}
            alt={parsedDetailsData.name}
            width="8rem"
          />
          <div>
            <span>
              {parsedDetailsData.name} #{parsedDetailsData.numericCode}
            </span>
            <span>Capital: {parsedDetailsData.capital}</span>
          </div>
        </MainInfoContainer>
        {!isEditMode && (
          <SecondaryInfoContainer>
            <div>
              <span>Area</span>
              <span>{formatNumberToUS(parsedDetailsData.area)}</span>
            </div>

            <div>
              <span>Population</span>
              <span>{formatNumberToUS(parsedDetailsData.population)}</span>
            </div>
            <div>
              <span>Top-level domains</span>
              <span>
                {parsedDetailsData.topLevelDomains.map(({ name }) => name).join(', ')}
              </span>
            </div>
          </SecondaryInfoContainer>
        )}
        {isEditMode && (
          <DetailsForm onSubmit={handleSubmitEdition} initialValues={parsedDetailsData} />
        )}
      </DetailsContainer>
    </>
  );
};
