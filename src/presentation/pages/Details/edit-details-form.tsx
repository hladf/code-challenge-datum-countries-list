import React from 'react';

import { Formik, Form } from 'formik';

import { CountryDetailsModel } from 'models';
import { Input } from 'presentation/components';

import { FormContainer, InputContainer } from './details.styles';

interface Props {
  initialValues: CountryDetailsModel;
  onSubmit: (values: CountryDetailsModel) => void;
}

const DetailsForm: React.FC<Props> = ({ initialValues, onSubmit }) => {
  return (
    <FormContainer>
      <h3>Edit country data</h3>
      <Formik
        onSubmit={(values, actions) => {
          onSubmit(values);
          actions.setSubmitting(false);
        }}
        initialValues={initialValues}
      >
        <Form>
          <div>
            <InputContainer>
              <label htmlFor="name">Name</label>
              <br />
              <Input name="name" id="name" placeholder="Name" />
            </InputContainer>
            <InputContainer>
              <label htmlFor="capital">Capital</label>
              <br />
              <Input name="capital" id="capital" placeholder="Capital" />
            </InputContainer>
            <InputContainer>
              <label htmlFor="area">Area</label>
              <br />
              <Input name="area" id="area" placeholder="Area" type="number" />
            </InputContainer>
            <InputContainer>
              <label htmlFor="population">Population</label>
              <br />
              <Input
                name="population"
                id="population"
                placeholder="Population"
                type="number"
              />
            </InputContainer>
            <InputContainer>
              <label htmlFor="flag.svgFile">Flag Image URL</label>
              <br />
              <Input name="flag.svgFile" id="flag.svgFile" placeholder="Flag Image URL" />
            </InputContainer>
          </div>
          <button type="submit">Save</button>
        </Form>
      </Formik>
    </FormContainer>
  );
};

export default DetailsForm;
