import styled from 'styled-components';

import { RoundedImage } from 'presentation/components';
import { borderAndShadowPrimary } from 'presentation/styles/predefined-styles';

export const DetailsContainer = styled.div`
  margin: 15px;
  padding: 20px;

  background-color: ${({ theme }) => theme.colors.white};
  border-radius: ${({ theme }) => theme.borderRadius.primary};
`;

export const MainInfoContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  img {
    border: 1.5px solid ${({ theme }) => theme.colors.lightGray};
    ${borderAndShadowPrimary}
  }

  div {
    display: flex;
    flex-direction: column;
    padding: 0;
    padding-left: 10px;

    span {
      color: ${({ theme }) => theme.colors.darkGray};
      font-size: ${({ theme }) => theme.fontSizes[16]};
      font-weight: 300;
    }

    span:first-child {
      color: black;
      font-size: ${({ theme }) => theme.fontSizes[20]};
      font-weight: 500;
    }

    @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
      padding: 0 20px;
    }
  }
`;

export const SecondaryInfoContainer = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  align-items: center;
  div {
    display: flex;
    flex-direction: column;
    padding: 0;
    padding-left: 10px;

    span {
      color: ${({ theme }) => theme.colors.darkGray};
      font-size: ${({ theme }) => theme.fontSizes[16]};
      font-weight: 300;
    }

    span:first-child {
      color: black;
      font-size: ${({ theme }) => theme.fontSizes[20]};
      font-weight: 500;
    }

    @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
      padding: 0 20px;
    }
  }
`;

export const FormContainer = styled.div`
  margin-top: 10px;

  form {
    div {
      display: grid;
      grid-template-columns: auto;
    }
  }

  br {
    display: none;
  }

  label {
    margin: 10px;
  }

  button {
    margin-top: 10px;
    align-self: center;
    width: 150px;
  }

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
    br {
      display: initial;
    }

    form {
      div {
        display: grid;
        grid-template-columns: auto auto;
      }
    }
  }
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const EditButton = styled(RoundedImage).attrs(() => ({
  style: { marginLeft: 'auto', border: 'none' },
}))`
  margin-left: auto;
  cursor: pointer;
  border: none;
  box-shadow: none;
  padding: 2px;
`;
