import styled from 'styled-components';

export const SectionContainer = styled.section`
  ul {
    margin-top: 3px;
    margin-bottom: 3px;
  }
`;
