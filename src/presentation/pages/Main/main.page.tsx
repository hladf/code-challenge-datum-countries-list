import React, { useCallback } from 'react';

import { CountryModel } from 'models';
import { CountryList, Header, Loading } from 'presentation/components';
import { useGetCountriesByName, useSelectedCountry } from 'presentation/hooks';

import { SectionContainer } from './main.styles';

export const Main = () => {
  const { loading, data, refetch } = useGetCountriesByName({});
  const { setSelectedCountryCode } = useSelectedCountry();

  const handleSearch = (param: string) => {
    refetch({ name: param });
  };

  const handleClickCard = useCallback(({ numericCode }: CountryModel) => {
    setSelectedCountryCode(numericCode);
  }, []);

  return (
    <SectionContainer>
      <Header onSearch={handleSearch} />
      {loading && <Loading />}
      {!loading && data?.countries && (
        <CountryList countries={data.countries} onClickCard={handleClickCard} />
      )}
    </SectionContainer>
  );
};
