import { useQuery } from '@apollo/client';

import { GET_COUNTRIES_BY_NAME } from 'main/graphql/operations/queries';
import { CountriesResultModel, QueryGetCountriesByNameVariables } from 'models';

export const useGetCountriesByName = ({ name }: QueryGetCountriesByNameVariables) => {
  return useQuery<CountriesResultModel, QueryGetCountriesByNameVariables>(
    GET_COUNTRIES_BY_NAME,
    name ? { variables: { name } } : undefined,
  );
};
