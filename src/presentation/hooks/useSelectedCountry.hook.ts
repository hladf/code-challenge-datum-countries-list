import { useMutation, useQuery } from '@apollo/client';
import { useCallback } from 'react';

import { EDIT_COUNTRY } from 'main/graphql/operations/mutations';
import {
  GET_LOCAL_COUNTRIES,
  GET_SELECTED_COUNTRY_CODE,
} from 'main/graphql/operations/queries';
import {
  CountryDetailsModel,
  LocalCountriesListModel,
  SelectedCountryModel,
} from 'models';

export const useSelectedCountry = () => {
  const { client, data: getSelectedCountryCode } = useQuery<SelectedCountryModel>(
    GET_SELECTED_COUNTRY_CODE,
  );
  const { data: getLocalCountries, refetch: refetchLocalCountries } =
    useQuery<LocalCountriesListModel>(GET_LOCAL_COUNTRIES);
  const [mutate] = useMutation(EDIT_COUNTRY);

  const setSelectedCountryCode = useCallback(
    (numericCode: string) => {
      client.writeQuery({ query: GET_SELECTED_COUNTRY_CODE, data: { numericCode } });
    },
    [client],
  );

  const getSelectedCountryLocalData = (numericCode: string) => {
    return getLocalCountries?.localCountries.find(
      (country) => country.numericCode === numericCode,
    );
  };
  const setSelectedCountryLocalData = (newValue: CountryDetailsModel) => {
    mutate({ variables: { country: newValue } });
  };

  return {
    getSelectedCountryCode,
    setSelectedCountryCode,
    getSelectedCountryLocalData,
    setSelectedCountryLocalData,
    refetchLocalCountries,
  };
};
