import { render, screen } from '@testing-library/react';
import React from 'react';

import userEvent, { specialChars } from '@testing-library/user-event';

import { MockedProvidersContainer } from 'main/tests';

import { Header } from './header.component';
describe('components', () => {
  it('Country Card component test', () => {
    const handleSearch = jest.fn();
    const searchString = 'brazil';
    const component = (
      <MockedProvidersContainer>
        <Header onSearch={handleSearch} />
      </MockedProvidersContainer>
    );
    render(component);

    const searchInput = screen.getByRole('searchbox', {
      name: /press 'enter' to search/i,
    });

    userEvent.type(searchInput, searchString + specialChars.enter);

    expect(handleSearch).toHaveBeenCalledWith(searchString);
    expect(
      screen.getByRole('heading', {
        name: /countries library/i,
      }),
    ).toBeInTheDocument();
  });
});
