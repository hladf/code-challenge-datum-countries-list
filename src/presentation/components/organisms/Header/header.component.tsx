import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';

import { mainRoutes } from 'main/router';
import { SearchInput } from 'presentation/components';

import { HeaderContainer, TitleContainer, SearchContainer } from './header.styles';

interface Props {
  onSearch?: (param: string) => void;
}

export const Header: React.FC<Props> = ({ onSearch }) => {
  const handleKeyPress = useCallback<React.KeyboardEventHandler<HTMLInputElement>>(
    ({ key, currentTarget }) => {
      key === 'Enter' && onSearch && onSearch(currentTarget.value);
    },
    [],
  );

  const handleOnBlur = useCallback<React.FocusEventHandler<HTMLInputElement>>(
    ({ currentTarget }) => {
      onSearch && onSearch(currentTarget.value);
    },
    [],
  );

  return (
    <header>
      <HeaderContainer>
        <Link to={mainRoutes.Main}>
          <TitleContainer>
            <h2>Countries Library</h2>
          </TitleContainer>
        </Link>
        {onSearch && (
          <SearchContainer>
            <SearchInput
              name="search-input"
              onKeyPress={handleKeyPress}
              onBlur={handleOnBlur}
              title="Press 'Enter' to search"
              placeholder="Search a country here!"
              maxLength={20}
            />
          </SearchContainer>
        )}
      </HeaderContainer>
    </header>
  );
};
