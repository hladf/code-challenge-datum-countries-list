import styled from 'styled-components';

import { Images } from 'presentation/assets';
import { borderAndShadowStrong } from 'presentation/styles/predefined-styles';

export const HeaderContainer = styled.div`
  border-radius: ${({ theme }) => theme.borderRadius.primary};
  box-shadow: ${({ theme }) => theme.boxShadows.secondaryStrong}80;
  border-radius: 0 0 8px 8px;
  transition: all 0.3s linear;
  display: flex;
  flex-direction: column;

  background-image: linear-gradient(to right, #00000080, transparent),
    url(${Images.MountainsLandscape});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-color: white;

  padding: 1rem;
  align-items: center;
  margin: 0;

  h2 {
    margin: 10px;
  }

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
    flex-direction: row;
  }
`;

export const TitleContainer = styled.div`
  * {
    transition: all 0.5s linear;
  }
  position: relative;
  padding-left: 15px;
  h2 {
    ${borderAndShadowStrong}
    border-radius: ${({ theme }) => theme.borderRadius.secondary};
    position: relative;
    background-image: linear-gradient(to right, #000000, #00000095);
    padding: 2px 6px;
    z-index: 3;
    color: #eee;

    &:hover {
      filter: gray; /* IE6-9 */
      -webkit-filter: grayscale(70%); /* Chrome 19+ & Safari 6+ */
    }
  }

  h2::before {
    ${borderAndShadowStrong}
    content: '';
    width: 2.75rem;
    height: 2.75rem;
    border-radius: 50%;
    background-color: #000000;
    position: absolute;
    top: -6px;
    left: -36px;
    z-index: -1;
    background-image: url(${Images.GlobeIcon});
    background-repeat: no-repeat;
    background-size: 70%;
    background-position-x: 50%;
    background-position-y: 50%;
  }
`;

export const SearchContainer = styled.div`
  margin-top: 10px;
  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
    margin-left: auto;
    margin-top: 0;
  }
`;
