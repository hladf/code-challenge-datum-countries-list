import React from 'react';

import { CountryModel } from 'models';
import { useSelectedCountry } from 'presentation/hooks';

import { CountryCard } from '..';

import { ListContainer } from './country-list.styles';

type CountryListProps = {
  countries: CountryModel[];
  onClickCard?: (countryData: CountryModel) => void;
};

export const CountryList: React.FC<CountryListProps> = ({ countries, onClickCard }) => {
  const { getSelectedCountryLocalData } = useSelectedCountry();

  return (
    <ListContainer>
      {countries.map((country) => {
        const localData = getSelectedCountryLocalData(country.numericCode);

        return (
          <CountryCard
            key={country.numericCode}
            name={localData?.name || country.name}
            capital={localData?.capital || country.capital}
            flagSvg={localData?.flag.svgFile || country.flag.svgFile}
            href={'/details'}
            onClick={onClickCard ? () => onClickCard(country) : undefined}
          />
        );
      })}
    </ListContainer>
  );
};
