import { act, render, screen } from '@testing-library/react';
import React from 'react';

import user from '@testing-library/user-event';

import { CountryQueriesMock } from 'main/graphql/operations/queries/test';
import { MockedProvidersContainer } from 'main/tests';
import { mockCountryList } from 'models/test';

import { CountryList } from './country-list.component';

describe('components', () => {
  it('Country List component test', async () => {
    const promise = Promise.resolve();
    const handleClickCard = jest.fn();

    const countryName = 'Brazil';
    const component = (
      <MockedProvidersContainer mocks={[CountryQueriesMock.GET_LOCAL_COUNTRIES_MOCK]}>
        <CountryList countries={mockCountryList} onClickCard={handleClickCard} />
      </MockedProvidersContainer>
    );
    render(component);

    const link = screen.getByRole('link', {
      name: /^brazil/i,
    });

    user.click(link);
    expect(handleClickCard).toHaveBeenCalledWith(mockCountryList[0]);

    expect(screen.getByText(countryName)).toBeInTheDocument();
    expect(screen.getByText('Brasília')).toBeInTheDocument();
    expect(screen.getByAltText(/brazil/i)).toBeInTheDocument();
    await act(() => promise);
  });
});
