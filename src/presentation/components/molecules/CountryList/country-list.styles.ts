import styled from 'styled-components';

export const ListContainer = styled.ul`
  padding: 5px 10px;
  overflow: auto;

  li {
    margin: 10px 0;
  }

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
    padding: 10px 30px;
  }
`;
