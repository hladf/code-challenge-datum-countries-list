import styled from 'styled-components';

import {
  borderAndShadowPrimary,
  hoverEffectPrimary,
} from 'presentation/styles/predefined-styles';

export const CardContainer = styled.li`
  ${borderAndShadowPrimary}
  ${hoverEffectPrimary}
  display: flex;

  background-color: white;
  padding: 1rem;
  align-items: center;

  img {
    border: 1.5px solid ${({ theme }) => theme.colors.lightGray};
    ${borderAndShadowPrimary}
  }
`;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0;
  padding-left: 10px;
  span {
    color: ${({ theme }) => theme.colors.darkGray};
    font-size: ${({ theme }) => theme.fontSizes[14]};
    font-weight: 300;
  }
  span:first-child {
    color: black;
    font-size: ${({ theme }) => theme.fontSizes[16]};
    font-weight: 500;
  }

  @media only screen and (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
    padding: 0 20px;
  }
`;
