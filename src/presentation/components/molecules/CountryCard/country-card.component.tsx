import React from 'react';
import { Link } from 'react-router-dom';

import { RoundedImage } from 'presentation/components';

import { CardContainer, InfoContainer } from './country-card.styles';

type CountryCardProps = {
  name: string;
  capital: string;
  flagSvg: string;
  onClick?: () => void;
  href?: string;
};

export const CountryCard: React.FC<CountryCardProps> = ({
  name,
  flagSvg,
  capital,
  href,
  onClick = () => {},
}) => {
  const CardElement = (
    <CardContainer>
      <RoundedImage src={flagSvg} alt={name} width={'4rem'} />
      <InfoContainer>
        <span>{name}</span>
        <span>{capital}</span>
      </InfoContainer>
    </CardContainer>
  );
  return href ? (
    <Link to={href} onClick={onClick}>
      {CardElement}
    </Link>
  ) : (
    CardElement
  );
};
