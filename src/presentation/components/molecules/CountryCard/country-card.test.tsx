import { render, screen } from '@testing-library/react';
import React from 'react';

import { MockedProvidersContainer } from 'main/tests';

import { CountryCard } from './country-card.component';
describe('components', () => {
  it('Country Card component test', () => {
    const countryName = 'countryName';
    const component = (
      <MockedProvidersContainer>
        <CountryCard name={countryName} capital="capital" flagSvg="flag" />
      </MockedProvidersContainer>
    );
    render(component);

    expect(screen.getByText(countryName)).toBeInTheDocument();
    expect(screen.getByText('capital')).toBeInTheDocument();
    expect(
      screen.getByRole('img', {
        name: new RegExp(countryName),
      }),
    ).toBeInTheDocument();
  });
});
