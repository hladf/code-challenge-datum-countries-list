import { render, screen } from '@testing-library/react';
import React from 'react';

import { Formik, Form } from 'formik';

import { MockedProvidersContainer } from 'main/tests';

import { Input } from './input.component';
describe('components', () => {
  it('Input component render test', () => {
    const name = 'inputName';
    const component = (
      <MockedProvidersContainer>
        <Formik initialValues={{ [name]: '' }} onSubmit={() => {}}>
          <Form>
            <Input name={name} />
          </Form>
        </Formik>
      </MockedProvidersContainer>
    );
    render(component);

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });
});
