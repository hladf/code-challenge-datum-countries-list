import { Field } from 'formik';
import styled from 'styled-components';

export const FormikFieldStyled = styled(Field)`
  border-bottom: 2px solid ${({ theme }) => theme.colors.lightGray};
`;
