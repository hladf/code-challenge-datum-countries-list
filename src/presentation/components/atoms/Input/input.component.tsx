import React from 'react';

import { FormikFieldStyled } from './input.styles';

export const Input: React.FC<React.InputHTMLAttributes<HTMLInputElement>> = (props) => {
  return <FormikFieldStyled {...props} />;
};
