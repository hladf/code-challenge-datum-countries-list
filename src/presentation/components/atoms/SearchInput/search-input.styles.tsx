import styled from 'styled-components';

import { Images } from 'presentation/assets';
import { borderAndShadowPrimary } from 'presentation/styles/predefined-styles';

export const Container = styled.div`
  input {
    ${borderAndShadowPrimary}
    border-radius: ${({ theme }) => theme.borderRadius.secondary};
    border: 1px solid ${({ theme }) => theme.colors.lightGray};
    padding: 10px;
    padding-left: 23px;
    outline: none;
    background-image: url(${Images.LupeIcon});
    background-repeat: no-repeat;
    background-size: 20px;
    background-position-y: 50%;
    background-position-x: 2px;
  }
`;
