import React from 'react';

import { Container } from './search-input.styles';

export const SearchInput: React.FC<
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>
> = ({ ...rest }) => {
  return (
    <Container>
      <input type="search" {...rest} />
    </Container>
  );
};
