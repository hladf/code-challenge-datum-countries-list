import React from 'react';

import { StyledLoading } from './loading.styles';

export const Loading: React.FC = () => {
  return <StyledLoading data-testid="loading-spinner"></StyledLoading>;
};
