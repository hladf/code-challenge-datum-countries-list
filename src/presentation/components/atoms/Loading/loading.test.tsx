import { render, screen } from '@testing-library/react';
import React from 'react';

import { MockedProvidersContainer } from 'main/tests';

import { Loading } from './loading.component';
describe('components', () => {
  it('Input component render test', () => {
    const component = (
      <MockedProvidersContainer>
        <Loading />
      </MockedProvidersContainer>
    );
    render(component);

    expect(screen.getByTestId('loading-spinner')).toBeInTheDocument();
  });
});
