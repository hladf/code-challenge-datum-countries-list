import styled, { keyframes } from 'styled-components';

import { Images } from 'presentation/assets';

const spin = keyframes`
  from {transform:rotate(0deg);}
  to {transform:rotate(359deg);}
`;

export const StyledLoading = styled.div`
  width: 25px;
  height: 25px;
  background-size: contain;
  background-image: url('${Images.LoadingIcon}');
  animation: ${spin} 1s infinite linear;
  margin: 20px auto;
`;
