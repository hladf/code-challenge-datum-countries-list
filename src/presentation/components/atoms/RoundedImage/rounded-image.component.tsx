import React from 'react';

import { FigureContainer } from './rounded-image.styles';

export const RoundedImage: React.FC<
  React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>
> = ({ src = '', alt = '', height = 'initial', width = '3rem', style = {}, ...rest }) => {
  return (
    <FigureContainer style={{ height, width, minWidth: width, ...style }}>
      <img src={src} alt={alt} {...rest} />
    </FigureContainer>
  );
};
