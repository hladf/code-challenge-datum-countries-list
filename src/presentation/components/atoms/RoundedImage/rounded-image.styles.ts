import styled from 'styled-components';

export const FigureContainer = styled.figure`
  min-width: 50px;
  margin: 0;
  border-radius: ${({ theme }) => theme.borderRadius.primary};
  height: auto;
  img {
    max-width: 100%;
    max-height: 100%;
    border-radius: ${({ theme }) => theme.borderRadius.primary};
  }
`;
