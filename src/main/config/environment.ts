// This file is used to centralize environment variables, but in this case, we dont need .env

/** Trying to avoid problems with cors and HTTPS */
const isHTTPS = window?.location.origin.includes('https');

console.log({ isHTTPS, origin: window?.location.origin });

export const GRAPHQL_URI = `http${isHTTPS ? 's' : ''}://testefront.dev.softplan.com.br/`;
