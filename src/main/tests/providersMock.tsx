import { ApolloCache } from '@apollo/client';
import { MockedProvider, MockedResponse } from '@apollo/client/testing';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { ThemeProvider } from 'styled-components';

import GlobalStyle from 'presentation/styles/global';
import { theme } from 'presentation/styles/theme';

type Props = {
  mocks?: readonly MockedResponse<Record<string, any>>[];
  cache?: ApolloCache<{}>;
  children: React.ReactNode;
};

export const MockedProvidersContainer = ({ mocks, children, cache }: Props) => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <>
          <MockedProvider mocks={mocks} addTypename={false} cache={cache}>
            {children}
          </MockedProvider>
          <GlobalStyle />
        </>
      </ThemeProvider>
    </BrowserRouter>
  );
};
