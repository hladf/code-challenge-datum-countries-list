import { formatNumberToUS, mergeWithoutDuplicates } from '.';

describe('main/helpers', () => {
  it('Should merge object with array without duplicates', () => {
    const newItem = { name: 'name 2' };
    const mock = [{ name: 'name 1' }, newItem];
    const mergedData = mergeWithoutDuplicates<any>(mock, newItem, 'name');
    expect(mergedData).toEqual(expect.arrayContaining([newItem]));
    expect(mergedData).toHaveLength(2);
  });

  it('Should format number to US format', () => {
    const formatedNumber = formatNumberToUS(999999);
    expect(formatedNumber).toBe('999,999');
  });
});
