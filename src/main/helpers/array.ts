export function mergeWithoutDuplicates<T>(array: T[], newValue: T, keyToSearch: string) {
  return array.reduce<T[]>((previousValue, currentValue) => {
    const indexOf = (array: T[], search: any) =>
      array
        .map((item: any) => typeof item === 'object' && item[keyToSearch])
        .indexOf(typeof search === 'object' && search[keyToSearch]);
    if (indexOf(previousValue, newValue) < 0) {
      previousValue.push(newValue);
    }
    if (indexOf(previousValue, currentValue) < 0) {
      previousValue.push(currentValue);
    }
    return previousValue;
  }, []);
}
