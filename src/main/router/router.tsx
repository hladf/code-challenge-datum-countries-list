import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { Main, Details } from 'presentation/pages';

import { mainRoutes } from './routes';

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path={mainRoutes.Details} component={Details} />
        <Route path={mainRoutes.Main} exact component={Main} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
