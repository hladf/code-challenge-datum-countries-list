import { gql } from '@apollo/client';

export const GET_COUNTRIES_BY_NAME = gql`
  query GetCountriesByName($name: String) {
    countries: Country(first: 30, orderBy: name_asc, filter: { name_contains: $name }) {
      name
      numericCode
      capital
      flag {
        svgFile
      }
    }
  }
`;

export const GET_COUNTRY_DETAILS = gql`
  query GetCountryByNumericCode($numericCode: String) {
    countryDetails: Country(first: 1, filter: { numericCode: $numericCode }) {
      name
      numericCode
      capital
      area
      population
      topLevelDomains {
        name
      }
      flag {
        svgFile
      }
    }
  }
`;

export const GET_SELECTED_COUNTRY_CODE = gql`
  query selectedCountryCode {
    numericCode @client
  }
`;

export const GET_LOCAL_COUNTRIES = gql`
  query GetLocalCountries {
    localCountries @client {
      name
      numericCode
      capital
      area
      population
      topLevelDomains {
        name
      }
      flag {
        svgFile
      }
    }
  }
`;
