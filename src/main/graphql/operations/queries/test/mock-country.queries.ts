import { MockedResponse } from '@apollo/client/testing';

import {
  GET_COUNTRY_DETAILS,
  GET_SELECTED_COUNTRY_CODE,
  GET_COUNTRIES_BY_NAME,
  GET_LOCAL_COUNTRIES,
} from '..';

type MockType = MockedResponse<Record<string, any>>;

interface MockOptions {
  GET_COUNTRIES_BY_NAME_MOCK: MockType;
  GET_LOCAL_COUNTRIES_MOCK: MockType;
  GET_COUNTRY_DETAILS_MOCK: MockType;
  GET_SELECTED_COUNTRY_CODE_MOCK: MockType;
}

export const CountryQueriesMock: MockOptions = {
  GET_SELECTED_COUNTRY_CODE_MOCK: {
    request: {
      query: GET_SELECTED_COUNTRY_CODE,
    },
    result: {
      data: {
        numericCode: '076',
      },
    },
  },
  GET_COUNTRY_DETAILS_MOCK: {
    request: {
      operationName: 'GetCountryByNumericCode',
      query: GET_COUNTRY_DETAILS,
      variables: {
        numericCode: '076',
      },
    },
    result: {
      data: {
        countryDetails: [
          {
            name: 'Brazil',
            numericCode: '076',
            capital: 'Brasília',
            area: 8515767,
            population: 206135893,
            topLevelDomains: [{ name: '.br' }],
            flag: {
              svgFile: 'https://restcountries.eu/data/bra.svg',
            },
          },
        ],
      },
    },
  },
  GET_LOCAL_COUNTRIES_MOCK: {
    request: {
      query: GET_LOCAL_COUNTRIES,
    },
    result: {
      data: {
        localCountries: [
          {
            name: 'Brazil modified',
            numericCode: '076',
            capital: 'Brasília',
            area: 8515767,
            population: 206135893,
            topLevelDomains: [
              {
                name: '.br',
              },
            ],
            flag: {
              svgFile: 'https://restcountries.eu/data/bra.svg',
            },
          },
        ],
      },
    },
  },
  GET_COUNTRIES_BY_NAME_MOCK: {
    request: {
      query: GET_COUNTRIES_BY_NAME,
      variables: {
        name: 'Br',
      },
    },
    result: {
      data: {
        countries: [
          {
            name: 'Brazil',
            numericCode: '076',
            capital: 'Brasília',
            area: 8515767,
            population: 206135893,
            topLevelDomains: [
              {
                name: '.br',
              },
            ],
            flag: {
              svgFile: 'https://restcountries.eu/data/bra.svg',
            },
          },
          {
            name: 'British Indian Ocean Territory',
            numericCode: '086',
            capital: 'Diego Garcia',
            area: 60,
            population: 3000,
            topLevelDomains: [
              {
                name: '.io',
              },
            ],
            flag: {
              svgFile: 'https://restcountries.eu/data/iot.svg',
            },
          },
          {
            name: 'Brunei Darussalam',
            numericCode: '096',
            capital: 'Bandar Seri Begawan',
            area: 5765,
            population: 411900,
            topLevelDomains: [
              {
                name: '.bn',
              },
            ],
            flag: {
              svgFile: 'https://restcountries.eu/data/brn.svg',
            },
          },
        ],
      },
    },
  },
};
