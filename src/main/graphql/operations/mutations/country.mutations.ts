import { gql } from '@apollo/client';

export const EDIT_COUNTRY = gql`
  mutation editCountry($country: Country) {
    editCountry(country: $country) @client
  }
`;
